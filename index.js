const {
    liveLog,
    dataTypeValidation,
    Str
    
} = require('./tools')

async function t(){
    const log = await new liveLog({
        server: 'http://localhost:3333',
        app: 'testApp',
        source:'application',
        token:'testkey'
    })
    log.success('connected')
}
function test(){
    console.log(Str.removeSpecial('2hello12||dsa^!^+^%+&%/&(/&('))
    const newid = Str.newId()
    console.log(newid)
    console.log(dataTypeValidation.assert.isUUID(1))
}
module.exports = {
    liveLog,
    dataTypeValidation,
    Str
}
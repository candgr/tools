const crypto = require('crypto')
const {
    isNumeric
} = require('./dataTypeValidation')
function removeSpecial (text) {
    if(text) {
      var lower = text.toLowerCase();
      var upper = text.toUpperCase();
      var result = "";
      for(var i=0; i<lower.length; ++i) {
        if(isNumeric(parseInt(text[i]))|| (lower[i] != upper[i]) || (lower[i].trim() === '')) {
          result += text[i];
        }
      }
      return result;
    }
    return '';
  }
function newId(){
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
      });
}
function newuid(){
    return Math.random().toString(26).slice(2)
}
module.exports = {
    removeSpecial,
    newId
}
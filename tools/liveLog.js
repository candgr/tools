const request = require('request')
const colors = require('colors')
colors.setTheme({
    info: 'cyan',
    warn: 'yellow',
    success: 'green',
    error: 'red'
  });

class liveLog {
    server
    app
    source
    time
    token
    io
    isConnected
    constructor({
        server,
        app,
        source,
        token
    }) {

        return (async () => {
            try {
                this.server = server
                this.app = app
                this.source = source
                this.token = token
                if(await this.healthCheck()){
                    await this.connect()
                }
                
                return this
            } catch (e) {
                console.log(e.message)
            }
        })()
    }

    async healthCheck() {
        request.get(`${this.server}/health-check`, (err, res, body) => {
            if (res && res.statusCode == 200) {
                //ok
                this.isConnected = true
            } else {
                this.isConnected = false
                // throw new Error('socket-is-down')
            }
            return this.isConnected
        })

    }

    async connect() {

        const io = require('socket.io-client')(this.server, {
            reconnect: true
        })

        this.io = io
    }

    setSource(source) {
        this.source = source
    }

    success(log) {
        if (this.isConnected) {
            this.io.emit('log-outer', JSON.stringify({
                app: this.app,
                source: this.source,
                log,
                type: 'success',
                time: Date.now().valueOf()
            }))
        } else {
            const message = this.source ? `${this.source}::${log}`.success.bold : log.success.bold
            console.log(message)
        }

    }

    error(log) {
        if (this.isConnected) {
            this.io.emit('log-outer', JSON.stringify({
                app: this.app,
                source: this.source,
                log,
                type: 'error',
                time: Date.now().valueOf()
            }))
        } else {
            const message = this.source ? `${this.source}::${log}`.error.bold : log.error.bold
            console.log(message)
        }

    }

    warn(log) {
        if (this.isConnected) {
            this.io.emit('log-outer', JSON.stringify({
                app: this.app,
                source: this.source,
                log,
                type: 'warn',
                time: Date.now().valueOf()
            }))
        } else {
            const message = this.source ? `${this.source}::${log}`.warn.bold : log.warn.bold
            console.log(message)
        }

    }
    trace(log) {
        if (this.isConnected) {
            this.io.emit('log-outer', JSON.stringify({
                app: this.app,
                source: this.source,
                log,
                type: 'trace',
                time: Date.now().valueOf()
            }))
        } else {
            const message = this.source ? `${this.source}::${log}`.info.bold : log.info.bold
            console.log(message)
        }

    }
}
module.exports = liveLog